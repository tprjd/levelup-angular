<div style="margin: auto; width: 50%; color: #222222; font-family: sans-serif;">
  <h1 style="text-align: center">Typescript</h1>
  <section>
    <div>
      TypeScript is an open-source programming language developed and maintained by Microsoft. It is a strict
      syntactical
      superset of JavaScript, and adds optional static typing to the language.

      TypeScript is designed for development of large applications and transcompiles to JavaScript. As TypeScript is a
      superset of JavaScript, existing JavaScript programs are also valid TypeScript programs. TypeScript may be used to
      develop JavaScript applications for both client-side and server-side (Node.js) execution.

      There are multiple options available for transcompilation. Either the default TypeScript Checker can be used, or
      the
      Babel compiler can be invoked to convert TypeScript to JavaScript.

      TypeScript supports definition files that can contain type information of existing JavaScript libraries, much like
      C++ header files can describe the structure of existing object files. This enables other programs to use the
      values
      defined in the files as if they were statically typed TypeScript entities. There are third-party header files for
      popular libraries such as jQuery, MongoDB, and D3.js. TypeScript headers for the Node.js basic modules are also
      available, allowing development of Node.js programs within TypeScript.

      The TypeScript compiler is itself written in TypeScript and compiled to JavaScript. It is licensed under the
      Apache
      2.0 License.

      TypeScript is included as a first-class programming language in Microsoft Visual Studio 2013 Update 2 and later,
      beside C# and other Microsoft languages. An official extension allows Visual Studio 2012 to support TypeScript as
      well.

      Anders Hejlsberg, lead architect of C# and creator of Delphi and Turbo Pascal, has worked on the development of
      TypeScript.
    </div>
  </section>
  <section>
    <h2>History</h2>
    <div>
      TypeScript was first made public in October 2012 (at version 0.8), after two years of internal development at
      Microsoft. Soon after the announcement, Miguel de Icaza praised the language itself, but criticized the lack of
      mature IDE support apart from Microsoft Visual Studio, which was not available on Linux and OS X at that time.
      Today
      there is support in other IDEs, particularly in Eclipse, via a plug-in contributed by Palantir Technologies.
      Various
      text editors, including Emacs, Vim, Sublime, Webstorm, Atom and Microsoft's own Visual Studio Code also support
      TypeScript.

      TypeScript 0.9, released in 2013, added support for generics. TypeScript 1.0 was released at Microsoft's Build
      developer conference in 2014. Visual Studio 2013 Update 2 provides built-in support for TypeScript.

      In July 2014, the development team announced a new TypeScript compiler, claiming 5× performance gains.
      Simultaneously, the source code, which was initially hosted on CodePlex, was moved to GitHub.

      On 22 September 2016, TypeScript 2.0 was released; it introduced several features, including the ability for
      programmers to optionally prevent variables from being assigned null values, sometimes referred to as the
      billion-dollar mistake.
    </div>
  </section>
  <section>
    <h1>Language features</h1>
    <div>
      TypeScript originated from the shortcomings of JavaScript for the development of large-scale applications both at
      Microsoft and among their external customers. Challenges with dealing with complex JavaScript code led to demand
      for
      custom tooling to ease developing of components in the language.

      TypeScript developers sought a solution that would not break compatibility with the standard and its
      cross-platform
      support. Knowing that the current ECMAScript standard proposal promised future support for class-based
      programming,
      TypeScript was based on that proposal. That led to a JavaScript compiler with a set of syntactical language
      extensions, a superset based on the proposal, that transforms the extensions into regular JavaScript. In this
      sense
      TypeScript was a preview of what to expect of ECMAScript 2015. A unique aspect not in the proposal, but added to
      TypeScript, is optional static typing that enables static language analysis, which facilitates tooling and IDE
      support.

      ECMAScript 2015 support
      Main article: ECMAScript § 6th Edition - ECMAScript 2015
      TypeScript adds support for features such as classes, modules, and an arrow function syntax as defined in the
      ECMAScript 2015 standard.
    </div>
  </section>
  <section>
    <h2>Language features</h2>
    <div>
      TypeScript is a language extension that adds features to ECMAScript 6. Additional features include:
      <ul>
        <li>Type annotations and compile-time type checking</li>
        <li>Type inference</li>
        <li>Type erasure</li>
        <li>Interfaces</li>
        <li>Enumerated types</li>
        <li>Generics</li>
        <li>Namespaces</li>
        <li>Tuples</li>
        <li>Async/await</li>
      </ul>
      The following features are backported from ECMAScript 2015:
      <ul>
        <li>Classes</li>
        <li>Modules</li>
        <li>Abbreviated "arrow" syntax for anonymous functions</li>
        <li>Optional parameters and default parameters</li>
      </ul>

      Syntactically, TypeScript is very similar to JScript .NET, another Microsoft implementation of the ECMA-262
      language standard that added support for static typing and classical object-oriented language features such as
      classes, inheritance, interfaces, and namespaces.

      <p>Compatibility with JavaScript</p>
      TypeScript is a strict superset of ECMAScript 2015, which is itself a superset of ECMAScript 5, commonly referred
      to as JavaScript. As such, a JavaScript program is also a valid TypeScript program, and a TypeScript program can
      seamlessly consume JavaScript. By default the compiler targets ECMAScript 5, the current prevailing standard, but
      is also able to generate constructs used in ECMAScript 3 or 2015.

      With TypeScript, it is possible to use existing JavaScript code, incorporate popular JavaScript libraries, and
      call TypeScript-generated code from other JavaScript. Type declarations for these libraries are provided with the
      source code.

      <p>Type annotations</p>
      TypeScript provides static typing through type annotations to enable type checking at compile time. This is
      optional and can be ignored to use the regular dynamic typing of JavaScript.

      code snipet here

      The annotations for the primitive types are number, boolean and string. Weakly- or dynamically-typed structures
      are of type any.

      Type annotations can be exported to a separate declarations file to make type information available for TypeScript
      scripts using types already compiled into JavaScript. Annotations can be declared for an existing JavaScript
      library, as has been done for Node.js and jQuery.

      The TypeScript compiler makes use of type inference to infer types when types are not given. For example, the add
      method in the code above would be inferred as returning a number even if no return type annotation had been
      provided. This is based on the static types of left and right being numbers, and the compiler's knowledge that the
      result of adding two numbers is always a number. However, explicitly declaring the return type allows the compiler
      to verify correctness.

      If no type can be inferred because of lack of declarations, then it defaults to the dynamic any type. A value of
      the any type supports the same operations as a value in JavaScript and minimal static type checking is performed
      for operations on any values.

      <p>Declaration files</p>
      When a TypeScript script gets compiled there is an option to generate a declaration file (with the extension
      .d.ts) that functions as an interface to the components in the compiled JavaScript. In the process the compiler
      strips away all function and method bodies and preserves only the signatures of the types that are exported. The
      resulting declaration file can then be used to describe the exported virtual TypeScript types of a JavaScript
      library or module when a third-party developer consumes it from TypeScript.

      The concept of declaration files is analogous to the concept of header file found in C/C++.

      code sniper here

      Type declaration files can be written by hand for existing JavaScript libraries, as has been done for jQuery and
      Node.js.

      Large collections of declaration files for popular JavaScript libraries are hosted on GitHub in DefinitelyTyped.

      <p>Classes</p>
      TypeScript supports ECMAScript 2015 classes that integrate the optional type annotations support.

      code sniper here

      <p>Generics</p>
      TypeScript supports generic programming.

      code sniper here

      <p>Modules and namespaces</p>
      TypeScript distinguishes between modules and namespaces. Both features in TypeScript support encapsulation of
      classes, interfaces, functions and variables into containers. Namespaces (formerly internal modules) utilize
      immediately-invoked function expression of JavaScript to encapsulate code, whereas modules (formerly external
      modules) leverage JavaScript library patterns to do so (AMD or CommonJS).
    </div>
  </section>
  <section>
    <h2>Development tools</h2>
    <div>
      <p>Compiler</p>
      The TypeScript compiler, named tsc, is written in TypeScript. As a result, it can be compiled into regular
      JavaScript and can then be executed in any JavaScript engine (e.g. a browser). The compiler package comes bundled
      with a script host that can execute the compiler. It is also available as a Node.js package that uses Node.js as a
      host.

      There is also an alpha version of a client-side compiler in JavaScript, which executes TypeScript code on the fly,
      upon page load.

      The current version of the compiler supports ECMAScript 5 by default. An option is allowed to target ECMAScript
      2015 to make use of language features exclusive to that version (e.g. generators). Classes, despite being part of
      the ECMAScript 2015 standard, are available in both modes.

      <p>IDE and editor support</p>
      Microsoft provides a plug-in for Visual Studio 2012 and WebMatrix, full integrated support in Visual Studio 2013,
      Visual Studio 2015, and basic text editor support for Sublime Text, Emacs and Vim.
      Visual Studio Code is a (mostly) open-source, cross-platform source code editor developed by Microsoft based on
      Electron. It supports TypeScript in addition to several other languages, and offers features like debugging and
      intelligent code completion.
      alm.tools is an open source cloud IDE for TypeScript built using TypeScript, ReactJS and TypeStyle.
      JetBrains supports TypeScript with code completion, refactoring and debugging in its IDEs built on IntelliJ
      platform, such as PhpStorm 6, WebStorm 6, and IntelliJ IDEA, as well as their Visual Studio Add-in and extension,
      ReSharper 8.1.
      Atom has a TypeScript Plugin by Basarat with support for code completion, navigation, formatting, and fast
      compilation.
      The online Cloud9 IDE and Codenvy support TypeScript.
      A plugin is available for the NetBeans IDE.
      A plugin is available for the Eclipse IDE (version Kepler)
      TypEcs is available for the Eclipse IDE.
      Microsoft provides a TypeScript Plugin for Sublime Text.
      The Cross Platform Cloud IDE Codeanywhere supports TypeScript.
      Webclipse An Eclipse plugin designed to develop TypeScript and Angular 2.
      Angular IDE A standalone IDE available via npm to develop TypeScript and Angular 2 applications, with integrated
      terminal support.
      Tide — TypeScript Interactive Development Environment for Emacs.
      Tsuquyomi - a Vim plugin which uses TSServer and provides features like code completion, navigation and syntax and
      semantic error checking.
      Integration with build automation tools
      Using plug-ins, TypeScript can be integrated with build automation tools, including Grunt (grunt-ts), Apache Maven
      (TypeScript Maven Plugin), Gulp (gulp-typescript) and Gradle (TypeScript Gradle Plugin).

      <p>Linting tools</p>
      TSLint scans TypeScript code for conformance to a set of standards and guidelines. ESLint, a standard JavaScript
      linter, also provided some support for TypeScript via community plugins. However, ESLint's inability to leverage
      TypeScript's language services precluded certain forms of semantic linting and program-wide analysis. In early
      2019, the TSLint team announced the linter's deprecation in favor of typescript-eslint, a joint effort of the
      TSLint, ESLint and TypeScript teams to consolidate linting under the ESLint umbrella for improved performance,
      community unity and developer accessibility.
    </div>
  </section>
</div>
